;(function(context) {

	//load all required scripts	
	require('./scripts/anchors.external.popup.js');
	require('./scripts/standard.accordion.js');
	require('./scripts/custom.select.js');
//	require('./scripts/date.inputs.js');
//	require('./scripts/aspect.ratio.js');
	require('./scripts/magnific.popup.js');
//	require('./scripts/responsive.video.js');
	require('./scripts/lazy.images.js');
	require('./scripts/tabs.js');
	require('./scripts/nav.js');
	require('./scripts/j.sticky.js');
//	require('./scripts/blocks.js');
//	require('./scripts/gmap.js');


	var tests;
	var debounce;
	var preventOverScroll;
	var imageLoader;
	var d;
	var swiper;
	
	if(context) {
		debounce = context.debounce;
		preventOverScroll = context.preventOverScroll;
		tests = context.tests;
		imageLoader = context.imageLoader;
		swiper = context.swiper;
	} else {
		debounce = require('./scripts/debounce.js');
		preventOverScroll = require('./scripts/preventOverScroll.js');
		tests = require('./scripts/tests.js');
		imageLoader = require('./scripts/image.loader.js');
		swiper = require('./scripts/swiper.js');
	}
	
	d = debounce();
	preventOverScroll($('div.nav')[0]);	
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));