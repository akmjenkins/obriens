;(function(context) {

	var 
		tests,
		methods,
		$document = $(document);
	
	
	if(context) {
		tests = context.tests;
	} else {
		tests = require('./tests.js');
	}
	
	methods = {
		
		getDateInputs: function() {
			return $('input.date-input');
		},
		
		update: function() {
			var self = this;
			this.getDateInputs().each(function() { 
				if( (!tests.touch() || !tests.dateinputs()) ) {
					self.applyDatePickerToEl($(this)); 
				} else {
					$(this).attr('type','date');
				}
			});
		},
		
		applyDatePickerToEl: function($el) {
			$el.removeClass('date-input');
			$el.datetimepicker($.extend($el.data(),{}));
		}
		
	};
	
	
	$document
		.on('updateTemplate.date ready',function() {
			methods.update();
		})
	
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));