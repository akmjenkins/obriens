<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-search.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">Search</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-search">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">

		<div class="search-results-header">
			<span class="search-results-query">
				Search Results for <span>Boat Watching</span>
			</span><!-- .search-result-query -->
			
			<small class="uc post-date">Post Date</small>
		</div><!-- .search-results-header -->
		
		<div class="search-results-body">
		
			<a class="search-result" href="#">
				
				<div class="search-result-thumb">
					<div class="lazybg" data-src="https://placekitten.com/g/500/500"></div>
				</div><!-- .search-result-thumb -->
				
				<div class="search-result-body">
					<p>Donec a magna enim. Aliquam sollicitudin ex sit  amet tristique semper. Integer eu nunc ornare.</p>
					<span class="button sm">Read More</span>
				</div><!-- .search-result-body -->
				
				<div class="search-result-date">
					3 days ago
				</div><!-- .search-result-date -->
				
			</a><!-- .search-result -->
			
			<a class="search-result" href="#">
				
				<div class="search-result-thumb">
					<div class="lazybg" data-src="https://placekitten.com/g/500/500"></div>
				</div><!-- .search-result-thumb -->
				
				<div class="search-result-body">
					<p>Donec a magna enim. Aliquam sollicitudin ex sit  amet tristique semper. Integer eu nunc ornare.</p>
					<span class="button sm">Read More</span>
				</div><!-- .search-result-body -->
				
				<div class="search-result-date">
					3 days ago
				</div><!-- .search-result-date -->
				
			</a><!-- .search-result -->
			
			<a class="search-result" href="#">
				
				<div class="search-result-body">
					<p>Donec a magna enim. Aliquam sollicitudin ex sit  amet tristique semper. Integer eu nunc ornare.</p>
					<span class="button sm">Read More</span>
				</div><!-- .search-result-body -->
				
				<div class="search-result-date">
					3 days ago
				</div><!-- .search-result-date -->
				
			</a><!-- .search-result -->
			
			<a class="search-result" href="#">
				
				<div class="search-result-thumb">
					<div class="lazybg" data-src="https://placekitten.com/g/500/500"></div>
				</div><!-- .search-result-thumb -->
				
				<div class="search-result-body">
					<p>Donec a magna enim. Aliquam sollicitudin ex sit  amet tristique semper. Integer eu nunc ornare.</p>
					<span class="button sm">Read More</span>
				</div><!-- .search-result-body -->
				
				<div class="search-result-date">
					3 days ago
				</div><!-- .search-result-date -->
				
			</a><!-- .search-result -->
			
			<a class="search-result" href="#">
				
				<div class="search-result-body">
					<p>Donec a magna enim. Aliquam sollicitudin ex sit  amet tristique semper. Integer eu nunc ornare.</p>
					<span class="button sm">Read More</span>
				</div><!-- .search-result-body -->
				
				<div class="search-result-date">
					3 days ago
				</div><!-- .search-result-date -->
				
			</a><!-- .search-result -->
		
		</div><!-- .search-results-body -->
		
		<div class="search-results-footer">
		
			<div class="search-results-controls">
				<button class="t-fa-abs fa-chevron-circle-left prev">Previous</button>
				<button class="t-fa-abs fa-chevron-circle-right next">Next</button>
			</div><!-- .search-controls -->
			
			<span class="search-results-counts">
				<span class="start-count">5</span> or <span class="total-count">27</span>
			</span><!-- .search-results-counts -->
		</div><!-- .search-results-footer -->
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>