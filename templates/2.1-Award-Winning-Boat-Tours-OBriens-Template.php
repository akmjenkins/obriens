<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-boat-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					
					<div class="hero-content">

						<h1 class="hero-title">Award Winning Boat Tour</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-ship">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">
	
		<div class="split-block">
		
			<div class="split-block-item">
				<div class="split-block-content">
				
					<div class="hgroup centered">
						<h3 class="hgroup-title">Award Winning Boat Tour</h3>
						<span class="hgroup-subtitle">Donec a magna enim. Aliquam sollicitudin ex sit amet tristique semper. Integer eu nunc ornare.</span>
					</div><!-- .hgroup -->
					
					<div class="article-body">
						
						<p>
							Cruise with us to the Witless Bay Ecological Reserve, home to the largest Atlantic Puffin colony in North America 
							(260,000 pairs) and millions of other seabirds. Enjoy a spectacular performance by Humpback Whales as they frolic, 
							breach and pectoral fin slap right before your very eyes. A show not to be missed! Breathe in the fresh ocean air 
							as you sail past 10,000 year old icebergs.
						</p>
						
						<p>
							Come for the whales, countless seabirds and Newfoundland charm. Come to experience our rugged coastline and 
							enjoy the famous O'Brien's hospitality.
						</p>
						
						<p>
							Ride in comfort during this two hour tour aboard one of our large passenger vessels. Each vessel is equipped with a fully 
							enclosed heated cabin with panoramic viewing, canteen, bar and restrooms.
						</p>

						<p>
							Wheelchair accessible.
						</p>
						
					</div><!-- .article-body -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
			<div class="split-block-item medium-bg">
				<div class="split-block-content">
				
					<div class="hgroup centered">
						<h3 class="hgroup-title">Book Today</h3>
						<span class="hgroup-subtitle">Award Winning Boat Tour</span>
					</div><!-- .hgroup -->
							
							
					<div class="grid rates-grid">
						<div class="col col-2 sm-col-1">
							<div class="item">
							
									<div class="rates-row">
										<span class="l">Adult:</span>
										<span class="r">$57.50</span>
									</div><!-- .row -->
									
									<div class="rates-row">
										<span class="l">Student (&lt;25)</span>
										<span class="r">$42.95</span>
									</div><!-- .row -->
									
									<div class="rates-row">
										<span class="l">Seniors (65+):</span>
										<span class="r">$49.95</span>
									</div><!-- .row -->
									
									<div class="rates-row">
										<span class="l">Youth (&lt;17):</span>
										<span class="r">$29.95</span>
									</div><!-- .row -->
									
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-2 sm-col-1">
							<div class="item">
							
								<div class="rates-row">
									<span class="l">Child (&lt; 9):</span>
									<span class="r">$24.95</span>
								</div><!-- .row -->
								
								<div class="rates-row">
									<span class="l">Infant (&lt; 2):</span>
									<span class="r">Free</span>
								</div><!-- .row -->
								
								<div class="rates-row">
									<span class="l">Family (2A + 2C*):</span>
									<span class="r">$145.00</span>
								</div><!-- .row -->
								
								<small>*Applies to children and youth. price will be adjusted at point of ticket purchase</small>
							
							</div><!-- .item -->
						</div><!-- .col -->
						
					</div><!-- .grid -->
					
					
						
					<div class="center">
						<a href="#" class="button">Book Now</a>
						<a href="#" class="button">View Schedule</a>
					</div><!-- .center -->
					
					<div class="gallery-grid grid">
						
						<!-- href links to full thumbnail -->
						<div class="col col-4 sm-col-2">
							<a href="../assets/dist/images/temp/gallery/gallery-1.jpg" class="mpopup gallery-item bounce" data-gallery="gallery-slug">
								<div class="lazybg img" data-src="../assets/dist/images/temp/gallery/gallery-1.jpg"></div>
							</a>
						</div><!-- .col -->
						
						<div class="col col-4 sm-col-2">
							<a href="../assets/dist/images/temp/gallery/gallery-2.jpg" class="mpopup gallery-item bounce" data-gallery="gallery-slug">
								<div class="lazybg img" data-src="../assets/dist/images/temp/gallery/gallery-2.jpg"></div>
							</a>
						</div><!-- .col -->
						
						<div class="col col-4 sm-col-2">
							<a href="../assets/dist/images/temp/gallery/gallery-3.jpg" class="mpopup gallery-item bounce" data-gallery="gallery-slug">
								<div class="lazybg img" data-src="../assets/dist/images/temp/gallery/gallery-3.jpg"></div>
							</a>
						</div><!-- .col -->
						
						<div class="col col-4 sm-col-2">
							<a href="../assets/dist/images/temp/gallery/gallery-4.jpg" class="mpopup gallery-item bounce" data-gallery="gallery-slug">
								<div class="lazybg img" data-src="../assets/dist/images/temp/gallery/gallery-4.jpg"></div>
							</a>
						</div><!-- .col -->
						
					</div><!-- .gallery-grid -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
		</div><!-- .split-block -->
		
		<?php include('inc/i-testimonial.php'); ?>
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>