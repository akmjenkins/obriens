<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-contact.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">Rates &amp; Schedules</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-calendar">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">
	
		<div class="tab-wrapper">
			<div class="tab-controls">
			
				<div class="pad-20">
					
					<div class="hgroup">
						<h4 class="valign hgroup-title">Choose a Tour:</h4>
						
						<div class="selector valign with-arrow">
							<select class="tab-controller">
								<option>Award Winning Boat Tour</option>
								<option>Coastal Adventure Tour</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->
						
					</div><!-- .hgroup -->
					
				</div><!-- .pad-20 -->
			
			</div><!-- .tab-controls -->
			<div class="tab-holder">
			
				<div class="tab selected">
				
					<div class="contact-grid grid eqh nopad">
						<div class="col">
							<div class="item pad-20">
							
								<div class="hgroup">
									<h3 class="hgroup-title">Prices</h3>
								</div><!-- .hgroup -->
								
								<div class="rates-row row">
									<span class="l">Adult:</span>
									<span class="r">$57.50</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Student (&lt;25)</span>
									<span class="r">$42.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Seniors (65+):</span>
									<span class="r">$49.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Youth (&lt;17):</span>
									<span class="r">$29.95</span>
								</div><!-- .row -->
									
								<div class="rates-row row">
									<span class="l">Child (&lt; 9):</span>
									<span class="r">$24.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Infant (&lt; 2):</span>
									<span class="r">Free</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Family (2A + 2C*):</span>
									<span class="r">$145.00</span>
								</div><!-- .row -->
								
								<br />
								
								<small class="block">*Applies to children and youth. price will be adjusted at point of ticket purchase</small>
								
								<br />
								
								<a href="#" class="button">Book Now</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item pad-20">
							
								<div class="hgroup">
									<h3 class="hgroup-title">Schedule</h3>
								</div><!-- .hgroup -->
								
								<div class="tour-schedule">
									<div class="tour-schedule-head tour-schedule-row">
										<span>9:30</span>
										<span>11:30</span>
										<span>2:00</span>
										<span>4:30</span>
									</div><!-- .tour-schedule-head -->
									
									<div class="tour-schedule-row" data-month="May">
										<span>&nbsp;</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>&nbsp;</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-row" data-month="June">
										<span>&nbsp;</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>&nbsp;</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-row" data-month="July">
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-row" data-month="Aug">
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-row" data-month="Sept">
										<span>&nbsp;</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>&nbsp;</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-footer">
										<small>July &amp; August  – Friday's 9:00, 11:00, 2:00, 4:30. No boat tour Sunday's at 9:30</small>
										
										<div class="cc-logos">
											<img src="../assets/dist/images/interac.svg" alt="interac logo">
											<img src="../assets/dist/images/visa.svg" alt="visa logo">
											<img src="../assets/dist/images/mastercard.svg" alt="mastercard logo">
										</div><!-- .c--logos -->
									</div><!-- .tour-schedule-footer -->
									
								</div><!-- .tour-schedule -->
								
								
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->

				</div><!-- .tab -->
				
				<div class="tab">
				
					<div class="contact-grid grid eqh nopad">
						<div class="col">
							<div class="item pad-20">
							
								<div class="hgroup">
									<h3 class="hgroup-title">Prices</h3>
								</div><!-- .hgroup -->
								
								<div class="rates-row row">
									<span class="l">Adult:</span>
									<span class="r">$84.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Student (&lt;25)</span>
									<span class="r">$42.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Seniors (65+):</span>
									<span class="r">$49.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Youth (&lt;17):</span>
									<span class="r">$29.95</span>
								</div><!-- .row -->
									
								<div class="rates-row row">
									<span class="l">Child (&lt; 9):</span>
									<span class="r">$24.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Infant (&lt; 2):</span>
									<span class="r">Free</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Family (2A + 2C*):</span>
									<span class="r">$145.00</span>
								</div><!-- .row -->
								
								<br />
								
								<small class="block">*Applies to children and youth. price will be adjusted at point of ticket purchase</small>
								
								<br />
								
								<a href="#" class="button">Book Now</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item pad-20">
							
								<div class="hgroup">
									<h3 class="hgroup-title">Schedule</h3>
								</div><!-- .hgroup -->
								
								<div class="tour-schedule">
									<div class="tour-schedule-head tour-schedule-row">
										<span>9:30</span>
										<span>11:30</span>
										<span>2:00</span>
										<span>4:30</span>
									</div><!-- .tour-schedule-head -->
									
									<div class="tour-schedule-row" data-month="May">
										<span>&nbsp;</span>
										<span>&nbsp;</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>&nbsp;</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-row" data-month="June">
										<span>&nbsp;</span>
										<span>&nbsp;</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>&nbsp;</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-row" data-month="July">
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-row" data-month="Aug">
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-row" data-month="Sept">
										<span>&nbsp;</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>
											<i class="t-fa-abs fa-check">Available</i>
										</span>
										<span>&nbsp;</span>
									</div><!-- .tour-schedule-row -->
									
									<div class="tour-schedule-footer">
										<small>July &amp; August  – Friday's 9:00, 11:00, 2:00, 4:30. No boat tour Sunday's at 9:30</small>
										
										<div class="cc-logos">
											<img src="../assets/dist/images/interac.svg" alt="interac logo">
											<img src="../assets/dist/images/visa.svg" alt="visa logo">
											<img src="../assets/dist/images/mastercard.svg" alt="mastercard logo">
										</div><!-- .c--logos -->
									</div><!-- .tour-schedule-footer -->
									
								</div><!-- .tour-schedule -->
								
								
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
					
				</div><!-- .tab -->
				
			</div><!-- .tab-holder -->
		</div><!-- .tab-wrapper -->
		
		<?php include('inc/i-testimonial.php'); ?>
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>