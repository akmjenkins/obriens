<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-boat-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">Award Winning Boat Tour</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-ship">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">
	
		<div class="split-block">
		
			<div class="split-block-item">
				<div class="split-block-content">
				
					<div class="center article-body">
				
						<div class="hgroup centered">
							<h3 class="hgroup-title">Maps &amp; Guides</h3>
						</div><!-- .hgroup -->
						
						<div class="lazybg with-img big-ico-wrap">
							<img class="big-ico" src="../assets/dist/images/temp/maps-guides.svg" alt="maps and guides">
						</div><!-- .lazybg.with-img -->
						
						<p>
							Neque porro quisquam est quidolorem ipsum quia dolor sit amet,consectetur, 
							adipisci velit nequeporro quisquam est qui doloremipsum quia dolor sit amet, 
							consectetur, adipisci velit neque porroquisquam est qui dolorem ipsumquia 
							dolor sit amet, consec- tetur,adipisci velit 
						</p>
					
						<a href="#" class="button">Select</a>
						
					</div><!-- .center -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
			<div class="split-block-item medium-bg">
				<div class="split-block-content">
				
					<div class="center article-body">
				
						<div class="hgroup centered">
							<h3 class="hgroup-title">Shuttle Service</h3>
						</div><!-- .hgroup -->
						
						<div class="lazybg with-img big-ico-wrap">
							<img class="big-ico" src="../assets/dist/images/temp/shuttle-service.svg" alt="shuttle service">
						</div><!-- .lazybg.with-img -->
						
						<p>
							Neque porro quisquam est quidolorem ipsum quia dolor sit amet,consectetur, 
							adipisci velit nequeporro quisquam est qui doloremipsum quia dolor sit amet, 
							consectetur, adipisci velit neque porroquisquam est qui dolorem ipsumquia 
							dolor sit amet, consec- tetur,adipisci velit 
						</p>
					
						<a href="#" class="button">Select</a>
						
					</div><!-- .center -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
		</div><!-- .split-block -->
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>