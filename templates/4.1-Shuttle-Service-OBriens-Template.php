<?php $bodyclass = 'map-page'; ?>
<?php include('inc/i-header.php'); ?>
	
	<div class="body">
	
		<div class="split-block">
			<div class="split-block-item">
				<div class="split-block-content">
					<div class="article-body">
				
						<div class="hgroup centered">
							<h3 class="hgroup-title">Shuttle Service</h3>
						</div><!-- .hgroup -->
						
						<div class="lazybg with-img big-ico-wrap">
							<img class="big-ico" src="../assets/dist/images/temp/shuttle-service.svg" alt="shuttle service">
						</div><!-- .lazybg.with-img -->
						
						<p>
							Cruise with us to the Witless Bay Ecological Reserve,
							home to the largest Atlantic Puffin colony in North
							America (260,000 pairs) and millions of other seabirds.
							Enjoy a spectacular performance by Humpback
							Whales as they frolic, breach and pectoral fin slap right
							before your very eyes. A show not to be missed!
							Breathe in the fresh ocean air as you sail past 10,000
							year old icebergs.
						</p>
						
						<p>
							Come for the whales, countless seabirds and New-
							foundland charm. Come to experience our rugged
							coastline and enjoy the famous O'Brien's hospitality.
						</p>
						
						<p>
							Ride in comfort during this two hour tour aboard one
							of our large passenger vessels. Each vessel is equipped
							with a fully enclosed heated cabin with panoramic
							viewing, canteen, bar and restrooms.				
						</p>
						
					</div><!-- .article-body -->
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			<div class="split-block-item">
				<div class="split-block-content">
					<div class="split-block-map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m29!1m12!1m3!1d345230.146880096!2d-53.07868886169375!3d47.46827565785715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m14!1i0!3e0!4m5!1s0x4b0ca68401047049%3A0xa8d84f93a4a1b81d!2sSt.John&#39;s+Airport%2C+World+Parkway%2C+Saint+John&#39;s%2C+NL!3m2!1d47.62121!2d-52.742442999999994!4m5!1s0x4b0ceb81e1cf77b3%3A0xb6c17389d78322!2sO&#39;Brien&#39;s+Whale+and+Bird+Tours%2C+22+Lower+Road%2C+Bay+Bulls%2C+NL+A0A+1C0!3m2!1d47.315171!2d-52.814237!5e0!3m2!1sen!2sca!4v1429579220954"frameborder="0" style="border:0"></iframe>
					</div><!-- .split-block-map -->
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
		</div><!-- .split-block -->
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>