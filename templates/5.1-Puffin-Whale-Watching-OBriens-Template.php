<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-whale.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">Puffin &amp; Whale Watching</h1>
						
						<div class="hero-hr">
							<span class="obriens-f ob-puffins">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Lorem ipsum dolor sit amet,consectetur adipiscing elit usce at sodales nibh.</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">
	
		<div class="split-block">
			<div class="split-block-item">
				<div class="split-block-content">
				
					<div class="article-body">
					
						<p>
							Lorem ipsum dolor sit amet,consectetur adipiscing elit.Fusce at sodales nibh. Inbibendum, metus vel faucibusporta, 
							est libero iaculis eros,sit amet porttitor qua m lacusac diam  In bibendum, metusvel faucibus Lorem ipsum dolor sit 
							amet,consectetur adipiscing elit.Fusce at sodales nibh. Inbibendum, metus vel faucibusporta, est libero iaculis 
							eros,sit amet porttitor qua m lacusac diam. 
						</p>

						<p>
							In bibendum, metusvel faucibus lorem ipsum dolor sit amet,consecte tur adipiscing elit. Fusce at sodales nibh. Inbibendum, 
							metus vel fau cibusporta, est libero iaculis eros,sit amet porttitor qua m lacusac diam  In bibendum, metusvel faucibus Lorem 
							ipsum dolor sit amet, consectetur adipiscing elit.Fusce at sodales nibh. Inbibendum, metus vel faucibusporta, est libero iaculis 
							eros,sit amet porttitor quam lacusac diam  In bibendum, metusvel faucibus
						</p>
						
						<p>
							Lorem ipsum dolor sit amet,consectetur adipiscing elit.Fusce at sodales nibh. Inbibendum, metus vel faucibusporta, 
							est libero iaculis eros,sit amet porttitor qua m lacusac diam  In bibendum, metusvel faucibus Lorem ipsum dolor sit 
							amet,consectetur adipiscing elit.Fusce at sodales nibh. Inbibendum, metus vel faucibusporta, est libero iaculis 
							eros,sit amet porttitor qua m lacusac diam. 
						</p>

						<p>
							In bibendum, metusvel faucibus lorem ipsum dolor sit amet,consecte tur adipiscing elit. Fusce at sodales nibh. Inbibendum, 
							metus vel fau cibusporta, est libero iaculis eros,sit amet porttitor qua m lacusac diam  In bibendum, metusvel faucibus Lorem 
							ipsum dolor sit amet, consectetur adipiscing elit.Fusce at sodales nibh. Inbibendum, metus vel faucibusporta, est libero iaculis 
							eros,sit amet porttitor quam lacusac diam  In bibendum, metusvel faucibus
						</p>
					
					</div><!-- .article-body -->
				
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			<div class="split-block-item noflex">
				
				<div class="split-block-gallery j-sticky" data-stuck-to=".body" data-unstick-at="950">
				
					<div class="swiper-wrapper">
						<div class="swiper big-gallery-swiper" 
							data-swipe="true"
							data-infinite="true" 
							data-arrows="false" 
							data-update-lazy-images="true">
						
							<div class="swipe-item">
								<div class="swipe-item-bg" data-src="../assets/dist/images/temp/gallery/gallery-1.jpg"></div>
							</div><!-- .swipe-item -->
							
							<div class="swipe-item">
								<div class="swipe-item-bg" data-src="../assets/dist/images/temp/gallery/gallery-2.jpg"></div>
							</div><!-- .swipe-item -->
							
							<div class="swipe-item">
								<div class="swipe-item-bg" data-src="../assets/dist/images/temp/gallery/gallery-3.jpg"></div>
							</div><!-- .swipe-item -->
							
						</div><!-- .swiper -->
					</div><!-- .swiper-wrapper -->
					
					<div class="swiper-wrapper">
						<div class="swiper little-gallery-swiper" 
							data-infinite="true" 
							data-slides-to-show="2" 
							data-update-lazy-images="true" 
							data-focus-on-select="true" 
							data-as-nav-for=".split-block-gallery .big-gallery-swiper">
						
							<div class="swipe-item">
								<div class="swipe-item-bg" data-src="../assets/dist/images/temp/gallery/gallery-1.jpg"></div>
							</div><!-- .swipe-item -->
							
							<div class="swipe-item">
								<div class="swipe-item-bg" data-src="../assets/dist/images/temp/gallery/gallery-2.jpg"></div>
							</div><!-- .swipe-item -->
							
							<div class="swipe-item">
								<div class="swipe-item-bg" data-src="../assets/dist/images/temp/gallery/gallery-3.jpg"></div>
							</div><!-- .swipe-item -->
							
						</div><!-- .swiper -->
					</div><!-- .swiper-wrapper -->
				
				</div><!-- .split-block-gallery -->
				
			</div><!-- .split-block-item -->
		</div><!-- .split-block -->
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>