<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-whale.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">FAQs</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-question">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">
	
		<div class="split-block split-block-faqs">
			<div class="split-block-item">
				
				<div class="acc with-indicators">

					<div class="acc-item">
					
						<div class="acc-item-handle">
							<span>
								Suspendisse potenti. Integer varius lobortis mauris, eget viverra erat efficitur ut Nam sagittis ipsum quam, id euismod lacus luctus eu?
							</span>
						</div><!-- .acc-item-handle -->
						
						<div class="acc-item-content">
							<p>
								Vivamus ut elit commodo, aliquet nunc nec, placerat tortor. Duis iaculis malesuada sodales. 
								Quisque eu convallis arcu. Aenean ipsum nisl, egestas eu mollis in, scelerisque vitae nibh. 
								Proin et pretium arcu, id eleifend nibh. Etiam fermentum metus quis tortor malesuada, 
								eget commodo libero euismod. Proin consequat pellentesque tellus eu pharetra. Ut et tellus 
								in metus dictum mollis eu vel tellus. Suspendisse molestie massa in ante ornare suscipit.
							</p>
						</div><!-- .acc-item-content -->
					</div><!-- .acc-item -->
				
					<div class="acc-item">
					
						<div class="acc-item-handle">
							<span>
								Suspendisse potenti. Integer varius lobortis mauris, eget viverra erat efficitur ut Nam sagittis ipsum quam, id euismod lacus luctus eu?
							</span>
						</div><!-- .acc-item-handle -->
						
						<div class="acc-item-content">
							<p>
								Vivamus ut elit commodo, aliquet nunc nec, placerat tortor. Duis iaculis malesuada sodales. 
								Quisque eu convallis arcu. Aenean ipsum nisl, egestas eu mollis in, scelerisque vitae nibh. 
								Proin et pretium arcu, id eleifend nibh. Etiam fermentum metus quis tortor malesuada, 
								eget commodo libero euismod. Proin consequat pellentesque tellus eu pharetra. Ut et tellus 
								in metus dictum mollis eu vel tellus. Suspendisse molestie massa in ante ornare suscipit.
							</p>
						</div><!-- .acc-item-content -->
					</div><!-- .acc-item -->
					
					<div class="acc-item">
					
						<div class="acc-item-handle">
							<span>
								Suspendisse potenti. Integer varius lobortis mauris, eget viverra erat efficitur ut Nam sagittis ipsum quam, id euismod lacus luctus eu?
							</span>
						</div><!-- .acc-item-handle -->
						
						<div class="acc-item-content">
							<p>
								Vivamus ut elit commodo, aliquet nunc nec, placerat tortor. Duis iaculis malesuada sodales. 
								Quisque eu convallis arcu. Aenean ipsum nisl, egestas eu mollis in, scelerisque vitae nibh. 
								Proin et pretium arcu, id eleifend nibh. Etiam fermentum metus quis tortor malesuada, 
								eget commodo libero euismod. Proin consequat pellentesque tellus eu pharetra. Ut et tellus 
								in metus dictum mollis eu vel tellus. Suspendisse molestie massa in ante ornare suscipit.
							</p>
						</div><!-- .acc-item-content -->
					</div><!-- .acc-item -->
					
					<div class="acc-item">
					
						<div class="acc-item-handle">
							<span>
								Suspendisse potenti. Integer varius lobortis mauris, eget viverra erat efficitur ut Nam sagittis ipsum quam, id euismod lacus luctus eu?
							</span>
						</div><!-- .acc-item-handle -->
						
						<div class="acc-item-content">
							<p>
								Vivamus ut elit commodo, aliquet nunc nec, placerat tortor. Duis iaculis malesuada sodales. 
								Quisque eu convallis arcu. Aenean ipsum nisl, egestas eu mollis in, scelerisque vitae nibh. 
								Proin et pretium arcu, id eleifend nibh. Etiam fermentum metus quis tortor malesuada, 
								eget commodo libero euismod. Proin consequat pellentesque tellus eu pharetra. Ut et tellus 
								in metus dictum mollis eu vel tellus. Suspendisse molestie massa in ante ornare suscipit.
							</p>
						</div><!-- .acc-item-content -->
					</div><!-- .acc-item -->
					
				</div><!-- .acc -->
				
			</div><!-- .split-block-item -->
			<div class="split-block-item">
				
				<div class="acc with-indicators">
				
					<div class="acc-item">
					
						<div class="acc-item-handle">
							<span>
								Suspendisse potenti. Integer varius lobortis mauris, eget viverra erat efficitur ut Nam sagittis ipsum quam, id euismod lacus luctus eu?
							</span>
						</div><!-- .acc-item-handle -->
						
						<div class="acc-item-content">
							<p>
								Vivamus ut elit commodo, aliquet nunc nec, placerat tortor. Duis iaculis malesuada sodales. 
								Quisque eu convallis arcu. Aenean ipsum nisl, egestas eu mollis in, scelerisque vitae nibh. 
								Proin et pretium arcu, id eleifend nibh. Etiam fermentum metus quis tortor malesuada, 
								eget commodo libero euismod. Proin consequat pellentesque tellus eu pharetra. Ut et tellus 
								in metus dictum mollis eu vel tellus. Suspendisse molestie massa in ante ornare suscipit.
							</p>
						</div><!-- .acc-item-content -->
					</div><!-- .acc-item -->
				
					<div class="acc-item">
					
						<div class="acc-item-handle">
							<span>
								Suspendisse potenti. Integer varius lobortis mauris, eget viverra erat efficitur ut Nam sagittis ipsum quam, id euismod lacus luctus eu?
							</span>
						</div><!-- .acc-item-handle -->
						
						<div class="acc-item-content">
							<p>
								Vivamus ut elit commodo, aliquet nunc nec, placerat tortor. Duis iaculis malesuada sodales. 
								Quisque eu convallis arcu. Aenean ipsum nisl, egestas eu mollis in, scelerisque vitae nibh. 
								Proin et pretium arcu, id eleifend nibh. Etiam fermentum metus quis tortor malesuada, 
								eget commodo libero euismod. Proin consequat pellentesque tellus eu pharetra. Ut et tellus 
								in metus dictum mollis eu vel tellus. Suspendisse molestie massa in ante ornare suscipit.
							</p>
						</div><!-- .acc-item-content -->
					</div><!-- .acc-item -->
					
					<div class="acc-item">
					
						<div class="acc-item-handle">
							<span>
								Suspendisse potenti. Integer varius lobortis mauris, eget viverra erat efficitur ut Nam sagittis ipsum quam, id euismod lacus luctus eu?
							</span>
						</div><!-- .acc-item-handle -->
						
						<div class="acc-item-content">
							<p>
								Vivamus ut elit commodo, aliquet nunc nec, placerat tortor. Duis iaculis malesuada sodales. 
								Quisque eu convallis arcu. Aenean ipsum nisl, egestas eu mollis in, scelerisque vitae nibh. 
								Proin et pretium arcu, id eleifend nibh. Etiam fermentum metus quis tortor malesuada, 
								eget commodo libero euismod. Proin consequat pellentesque tellus eu pharetra. Ut et tellus 
								in metus dictum mollis eu vel tellus. Suspendisse molestie massa in ante ornare suscipit.
							</p>
						</div><!-- .acc-item-content -->
					</div><!-- .acc-item -->
					
					<div class="acc-item">
					
						<div class="acc-item-handle">
							<span>
								Suspendisse potenti. Integer varius lobortis mauris, eget viverra erat efficitur ut Nam sagittis ipsum quam, id euismod lacus luctus eu?
							</span>
						</div><!-- .acc-item-handle -->
						
						<div class="acc-item-content">
							<p>
								Vivamus ut elit commodo, aliquet nunc nec, placerat tortor. Duis iaculis malesuada sodales. 
								Quisque eu convallis arcu. Aenean ipsum nisl, egestas eu mollis in, scelerisque vitae nibh. 
								Proin et pretium arcu, id eleifend nibh. Etiam fermentum metus quis tortor malesuada, 
								eget commodo libero euismod. Proin consequat pellentesque tellus eu pharetra. Ut et tellus 
								in metus dictum mollis eu vel tellus. Suspendisse molestie massa in ante ornare suscipit.
							</p>
						</div><!-- .acc-item-content -->
					</div><!-- .acc-item -->
					
				</div><!-- .acc -->
				
			</div><!-- .split-block-item -->
		</div><!-- .split-block -->
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>