<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-boat-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">Award Winning Boat Tour</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-ship">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">
	
		<div class="split-block">
		
			<div class="split-block-item">
				<div class="split-block-content">
				
					<div class="article-body">
				
						<div class="hgroup centered">
							<h4 class="hgroup-title">Tour 1: Award Winning Boat Tour</h4>
						</div><!-- .hgroup -->
						
						<hr class="centered" />
						
						<p>
							This is a take-your-breath-away outdoor adventure, topped off with a
							wink-of-the-eye sense of fun. Come for the whales, birds and the O'Brien's stories
							and insights into the wonders of this marine treasure.
						</p>
 
						<p>
							Join us for a two-hour cruise to the Witless Bay Ecological Reserve for a nautical
							adventure of a lifetime. Our 50-foot boat has rail space for everyone's viewing
							excitement. See whales, wheeling seabirds and soaring eagles, nothing compares
							to the thrill of meeting the wild world in such a personal way. It's an adventure you
							will never forget, only relive.
						</p>
						
						<div class="rates-row row">
							<span class="l">Adult:</span>
							<span class="r">$57.50</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Student (&lt;25)</span>
							<span class="r">$42.95</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Seniors (65+):</span>
							<span class="r">$49.95</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Youth (&lt;17):</span>
							<span class="r">$29.95</span>
						</div><!-- .row -->
							
						<div class="rates-row row">
							<span class="l">Child (&lt; 9):</span>
							<span class="r">$24.95</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Infant (&lt; 2):</span>
							<span class="r">Free</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Family (2A + 2C*):</span>
							<span class="r">$145.00</span>
						</div><!-- .row -->
						
						<br />
						
						<small class="block">*Applies to children and youth. price will be adjusted at point of ticket purchase</small>
						
						<br />
						
						<div class="center">
							<a href="#" class="button big">Book Now</a>
						</div><!-- .center -->
						
					</div><!-- .article-body -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
			<div class="split-block-item light-bg">
				<div class="split-block-content">
				
					<div class="article-body">
				
						<div class="hgroup centered">
							<h4 class="hgroup-title">Tour 2: Coast Adventure Boat Tour</h4>
						</div><!-- .hgroup -->
						
						<hr class="centered" />
						
						<p>
							This is a take-your-breath-away outdoor adventure, topped off with a
							wink-of-the-eye sense of fun. Come for the whales, birds and the O'Brien's stories
							and insights into the wonders of this marine treasure.
						</p>
 
						<p>
							Join us for a two-hour cruise to the Witless Bay Ecological Reserve for a nautical
							adventure of a lifetime. Our 50-foot boat has rail space for everyone's viewing
							excitement. See whales, wheeling seabirds and soaring eagles, nothing compares
							to the thrill of meeting the wild world in such a personal way. It's an adventure you
							will never forget, only relive.
						</p>
						
						<div class="rates-row row">
							<span class="l">Adult:</span>
							<span class="r">$57.50</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Student (&lt;25)</span>
							<span class="r">$42.95</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Seniors (65+):</span>
							<span class="r">$49.95</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Youth (&lt;17):</span>
							<span class="r">$29.95</span>
						</div><!-- .row -->
							
						<div class="rates-row row">
							<span class="l">Child (&lt; 9):</span>
							<span class="r">$24.95</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Infant (&lt; 2):</span>
							<span class="r">Free</span>
						</div><!-- .row -->
						
						<div class="rates-row row">
							<span class="l">Family (2A + 2C*):</span>
							<span class="r">$145.00</span>
						</div><!-- .row -->
						
						<br />
						
						<small class="block">*Applies to children and youth. price will be adjusted at point of ticket purchase</small>
						
						<br />
						
						<div class="center">
							<a href="#" class="button big">Book Now</a>
						</div><!-- .center -->
						
					</div><!-- .article-body -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
		</div><!-- .split-block -->
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>