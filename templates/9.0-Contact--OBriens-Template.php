<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-contact.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">Contact Us</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-envelope-o">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">

		<div class="contact-grid grid eqh nopad">
			<div class="col light-bg">
				<div class="item pad-40">
				
					<h2>We look forward to sailing with you!</h2>
					
					<div class="article-body">
					
						<address>
							O'Brien's Whale and Bird Tours Inc. <br />
							P.O. Box 300 <br />
							Bay Bulls, NL, Canada <br />
							A0A 1C0
						</address>
						
						<br />
						
						<div class="rows">
						
							<div class="row contact-row">
								<span class="l">Tel:</span>
								<span class="r">709-753-4850</span>
							</div><!-- .row -->
							
							<div class="row contact-row">
								<span class="l">Toll Free:</span>
								<span class="r">1-877-NF-WHALE</span>
							</div><!-- .row -->
							
							<div class="row contact-row">
								<span class="l">Fax:</span>
								<span class="r">709-334-2137</span>
							</div><!-- .row -->
							
							<div class="row contact-row">
								<span class="l">E-mail:</span>
								<span class="r"><a href="mailto:info@obriensboattours.com">info@obriensboattours.com</a></span>
							</div><!-- .row -->
							
						</div><!-- .rows -->
					
					</div><!-- .article-body -->
				
				</div><!-- .item -->
			</div><!-- .col -->
			<div class="col">
				<div class="item pad-20">
					
					<div class="grid">
					
						<div class="col col-2 sm-col-1">
							<div class="item pad-20">
							
								<div class="hgroup">
									<h3 class="hgroup-title">Book Today</h3>
									<span class="hgroup-subtitle">Award Winning Boat Tour</span>
								</div><!-- .hgroup -->
							
								<div class="rates-row row">
									<span class="l">Adult:</span>
									<span class="r">$57.50</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Student (&lt;25)</span>
									<span class="r">$42.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Seniors (65+):</span>
									<span class="r">$49.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Youth (&lt;17):</span>
									<span class="r">$29.95</span>
								</div><!-- .row -->
									
								<div class="rates-row row">
									<span class="l">Child (&lt; 9):</span>
									<span class="r">$24.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Infant (&lt; 2):</span>
									<span class="r">Free</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Family (2A + 2C*):</span>
									<span class="r">$145.00</span>
								</div><!-- .row -->
								
								<br />
								
								<small class="block">*Applies to children and youth. price will be adjusted at point of ticket purchase</small>
								
								<br />
								
								<a href="#" class="button">Book Now</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-2 sm-col-1">
							<div class="item pad-20">
							
								<div class="hgroup">
									<h3 class="hgroup-title">Book Today</h3>
									<span class="hgroup-subtitle">Coastal Adventure Tour</span>
								</div><!-- .hgroup -->
							
								<div class="rates-row row">
									<span class="l">Adult:</span>
									<span class="r">$84.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Student (&lt;25)</span>
									<span class="r">$42.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Seniors (65+):</span>
									<span class="r">$49.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Youth (&lt;17):</span>
									<span class="r">$29.95</span>
								</div><!-- .row -->
									
								<div class="rates-row row">
									<span class="l">Child (&lt; 9):</span>
									<span class="r">$24.95</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Infant (&lt; 2):</span>
									<span class="r">Free</span>
								</div><!-- .row -->
								
								<div class="rates-row row">
									<span class="l">Family (2A + 2C*):</span>
									<span class="r">$145.00</span>
								</div><!-- .row -->
								
								<br />
								
								<small class="block">*Applies to children and youth. price will be adjusted at point of ticket purchase</small>
								
								<br />
								
								<a href="#" class="button">Book Now</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						
					</div><!-- .grid -->
					
				</div><!-- .item -->
			</div><!-- .col -->
		</div><!-- .grid -->
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>