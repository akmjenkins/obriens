
				<div class="footer-wrap">
					<footer>
						
						<div class="footer-left">
							<a href="#" class="t-fa fa-calendar-o">Rates &amp; Schedules</a>
							<a href="#" class="t-fa fa-ship">Boat Tours</a>
						</div><!-- .footer-links -->
						
						<div class="footer-center">
							<a href="#" class="footer-link ob-icebergs">Icebergs</a>
							<a href="#" class="footer-link ob-whales">Whales</a>
							<a href="#" class="footer-link ob-puffins">Puffins</a>
							<a href="#" class="footer-link ob-coastline">Coastline</a>
						</div><!-- .footer-center -->
						
						<div class="footer-right">
							<?php include('inc/i-social.php'); ?>
						</div>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
						
					</footer>		
				</div><!-- .footer-wrap -->

			</div><!-- .body-wrap -->
			
		</div><!-- .page-wrapper -->

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/teamcanada',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>