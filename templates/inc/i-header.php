<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<html lang="en">

	<head>
		<title>O'Brien's Tours</title>
		<meta charset="utf-8">
		
		<!-- jQuery -->
		<script src="../assets/dist/lib/jquery/dist/jquery.min.js"></script>
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/dist/images/favicons/favicon.ico">
		<link rel="icon" type="image/png"  href="../assets/dist/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/dist/images/favicons/favicon-32.png" sizes="32x32">

		<!-- slickslider -->
		<link rel="stylesheet" href="../assets/dist/lib/slick.js/slick/slick.css">
		<script src="../assets/dist/lib/slick.js/slick/slick.min.js"></script>
		
		<!-- magnificpopup -->
		<link rel="stylesheet" href="../assets/dist/lib/magnific-popup/dist/magnific-popup.css">
		<script src="../assets/dist/lib/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
		
		<link rel="stylesheet" href="../assets/dist/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">
	
		<div class="page-wrapper">
	
			<!-- nav -->
			<?php include('inc/i-nav.php'); ?>

			<div class="body-wrap">
				<div class="mobile-nav-bg"></div>

				<?php include('inc/i-book.php'); ?>